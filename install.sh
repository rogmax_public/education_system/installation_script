#!/bin/bash

## curl -fsSL https://gitlab.com/rogmax_public/education_system/installation_script/snippets/1797052/raw -o get-app.sh && sudo bash get-app.sh


echo "Education system installation started"
pwd=$(pwd)

echo "Installing Git and Net tools"

    if [ -x "$(command -v apt)" ]; then
        sudo apt-get update -y
        sudo apt-get upgrade -y
        sudo apt install net-tools -y
        sudo apt-get install git -y
    else
        sudo yum upgrade
        sudo yum install net-tools -y
        sudo yum install git -y
    fi

    echo "Git and Net tools are installed"


if [ -x "$(command -v docker)" ]; then
    echo "Docker is installed"
    # command
else
    echo "Install docker"
    curl -fsSL https://get.docker.com -o get-docker.sh && sudo sh get-docker.sh
    echo "Docker is installed"
fi

sudo service docker start &> /dev/null
sudo docker stop db kms frontend backend &> /dev/null
sudo docker rm frontend backend &> /dev/null

sleep 8

if [ "$(netstat -lpn | grep :80)" ]; then
    echo "ERROR! Plese release 80 port"
    exit 1
elif [ "$(netstat -lpn | grep :8080)" ]; then
    echo "ERROR! Plese release 8080 port"
    exit 1
elif [ "$(netstat -lpn | grep :8888)" ]; then
    echo "ERROR! Plese release 8888 port"
    exit 1
   
fi

echo "To get credentials from our private repositories send email to maxmashnitsky@gmail.com"

echo "Enter username: "
read  username
echo "Enter password: "
read -s password

## checking if cridentials are correct

sudo service docker start &> /dev/null
sudo docker stop db kms frontend backend &> /dev/null
sudo docker rm frontend backend &> /dev/null
cd $pwd && sudo rm -rf backend frontend


if [ "$(sudo docker ps -a | grep db)" ]; then
    sudo docker start db
else 
    sudo docker run -v /opt/edu_sys_mysql:/var/lib/mysql --restart=always --name db -e MYSQL_ROOT_PASSWORD=w8f8h38fnldz -d mysql:5.7
    echo "MySQL DB is installed"
fi

if [ "$(sudo docker ps -a | grep kms)" ]; then
    sudo docker start kms
else 
    sudo docker run --restart=always -d --name kms -p 8888:8888 kurento/kurento-media-server
    echo "Kurento Media Server is installed"
fi

cd $pwd && git clone -b develop https://$username:$password@gitlab.com/rogmax/education_system/backend.git
cd $pwd/backend && sudo docker build -t edu_sys_backend .
cd $pwd/backend && sudo docker run -v /var/www/html/video:/var/www/html/video -itd --restart=always --link kms --name backend --link db -p 8080:8080 edu_sys_backend
echo "Backend is installed"

cd $pwd && git clone -b develop https://$username:$password@gitlab.com/rogmax/education_system/frontend.git
cd $pwd/frontend && sudo docker build -t edu_sys_frontend .
cd $pwd/frontend && sudo docker run -v /var/www/html/video:/var/www/html/video -itd --restart=always --name frontend -p 80:80 edu_sys_frontend

echo "Installed, check localhost:80 in your browser "
